from flask import Flask, app, render_template, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref, session
from sqlalchemy.sql.elements import literal
from werkzeug.utils import redirect
from flask import session

app = Flask(__name__)

# Esto sirve para algo
# Esto sirve para algo Esto está en LG braanch
#app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:root@localhost:5432/mibarrio.com.Flaskalchemy"

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://rnllzubcbxjxqb:03e149262153fb50e72b07467a64da4e4c1b2bde6dae9dd41f68bf3bb1efb91f@ec2-54-81-126-150.compute-1.amazonaws.com:5432/d2m0a5j71e4qrd'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.secret_key = 'clave super secreta'

db = SQLAlchemy(app)


class Marca(db.Model):
    __tablename__ = 'Marca'
    id = db.Column(db.Integer, primary_key=True)
    Nom_Marca = db.Column(db.String(20), unique=True, nullable=False)

    def __init__(self, Nom_Marca):
        self.Nom_Marca = Nom_Marca

    def __repr__(self):
        return f"<Marca {self.Nom_Marca}>"


class Categoria(db.Model):
    __tablename__ = 'Categoria'
    id = db.Column(db.Integer, primary_key=True)
    Nom_Categoria = db.Column(db.String(20), unique=True, nullable=False)

    def __init__(self, Nom_Categoria):
        self.Nom_Categoria = Nom_Categoria

    def __repr__(self):
        return f"<Categoria {self.Nom_Categoria}>"


class Proveedor(db.Model):
    __tablename__ = 'Proveedor'
    id = db.Column(db.Integer, primary_key=True)
    Nom_Proveedor = db.Column(db.String(20), unique=True, nullable=False)

    def __repr__(self):
        return f"<Proveedor {self.Nom_Proveedor}>"


class Producto(db.Model):
    __tablename__ = 'Producto'
    id = db.Column(db.Integer, primary_key=True)
    FK_Marca = db.Column(db.Integer, db.ForeignKey(
        'Marca.id'), unique=False, nullable=False)
    FK_Categoria = db.Column(db.Integer, db.ForeignKey(
        'Categoria.id'), unique=False, nullable=False)
    FK_Proveedor = db.Column(db.Integer, db.ForeignKey(
        'Proveedor.id'), unique=False, nullable=False)
    Nom_Producto = db.Column(db.String(20), unique=True, nullable=False)

    marca = db.relationship('Marca')
    categoria = db.relationship('Categoria')
    proveedor = db.relationship('Proveedor')

    def __repr__(self):
        return f"<Producto {self.Nom_Producto}>"


class Rol(db.Model):
    __tablename__ = 'Rol'
    id = db.Column(db.Integer, primary_key=True)
    Nom_Rol = db.Column(db.String(20), unique=True, nullable=False)

    def __repr__(self):
        return f"<Rol {self.Nom_Rol}>"


class Usuario(db.Model):
    __tablename__ = 'Usuario'
    id = db.Column(db.Integer, primary_key=True)
    FK_Rol = db.Column(db.Integer, db.ForeignKey(
        'Rol.id'), unique=True, nullable=False)
    Nom_Usuario = db.Column(db.String(20), unique=True, nullable=False)

    def __repr__(self):
        return f"<Usuario {self.Nom_Usuario}>"


class Inventario(db.Model):
    __tablename__ = 'Inventario'
    id = db.Column(db.Integer, primary_key=True)
    FK_Usuario = db.Column(db.Integer, db.ForeignKey(
        'Usuario.id'), unique=True, nullable=False)
    FK_Producto = db.Column(db.Integer, db.ForeignKey(
        'Producto.id'), unique=True, nullable=False)

    def __repr__(self):
        return f"<Producto {self.id}>"

# --------------------------------------------------------------------------------------
# Ruta inicial Index
# --------------------------------------------------------------------------------------


@app.route('/')
def index():
    return render_template('/index.html')


@app.route('/layout.html')
def base():
    return render_template('/base.html')
# --------------------------------------------------------------------------------------
# Ruta login
# --------------------------------------------------------------------------------------


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        if email == "lugadun@gmail.com" and password == "1234":
            session.clear()
            session["id"] = email
            session["rol"] = "admin"
            return redirect('/list_productos')
        else:
            return render_template('/login.html')
    else:
        session.clear()
        return render_template('/login.html')
# --------------------------------------------------------------------------------------
# Ruta inicial Marcas
# --------------------------------------------------------------------------------------


@app.route('/list_marcas', methods=['POST', 'GET'])
def op_marcas():
    if request.method == 'POST':
        nombre = request.form['nombre']
        insertmarca = Marca(Nom_Marca=nombre)
        db.session.add(insertmarca)
        db.session.commit()
        return redirect('/list_marcas')
    elif request.method == 'GET':
        marcas = Marca.query.all()
        results = [
            {
                "id": marca.id,
                "nombre": marca.Nom_Marca,
            } for marca in marcas]
        return render_template('Marcas.html', lista=results)


@app.route('/borrar_marca/<marca_id>')
def op_marca(marca_id):
    marca = Marca.query.get_or_404(marca_id)
    db.session.delete(marca)
    db.session.commit()
    return redirect('/list_marcas')


@app.route('/actualizar_marca/<marca_id>', methods=['GET', 'POST'])
def op_marca_update(marca_id):
    marca = Marca.query.get_or_404(marca_id)
    response = {
        "id": marca.id,
        "nombre": marca.Nom_Marca
    }
    if request.method == 'GET':
        return render_template('actualizar_marca.html', lista=response)
    elif request.method == 'POST':
        marca.Nom_Marca = request.form['nombre']
        db.session.add(marca)
        db.session.commit()
        return redirect('/list_marcas')

# --------------------------------------------------------------------------------------
# Ruta inicial Categorias
# --------------------------------------------------------------------------------------


@app.route('/list_categorias', methods=['POST', 'GET'])
def op_categorias():
    if request.method == 'POST':
        nombre = request.form['nombre']
        insertcategoria = Categoria(Nom_Categoria=nombre)
        db.session.add(insertcategoria)
        db.session.commit()
        return redirect('/list_categorias')
    elif request.method == 'GET':
        categorias = Categoria.query.all()
        results = [
            {
                "id": categoria.id,
                "nombre": categoria.Nom_Categoria,
            } for categoria in categorias]
        return render_template('Categorias.html', lista=results)


@app.route('/borrar_categoria/<categoria_id>')
def op_categoria(categoria_id):
    categoria = Categoria.query.get_or_404(categoria_id)
    db.session.delete(categoria)
    db.session.commit()
    return redirect('/list_categorias')


@app.route('/actualizar_categoria/<categoria_id>', methods=['GET', 'POST'])
def op_categoria_update(categoria_id):
    categoria = Categoria.query.get_or_404(categoria_id)
    response = {
        "id": categoria.id,
        "nombre": categoria.Nom_Categoria
    }
    if request.method == 'GET':
        return render_template('actualizar_categoria.html', lista=response)
    elif request.method == 'POST':
        categoria.Nom_Categoria = request.form['nombre']
        db.session.add(categoria)
        db.session.commit()
        return redirect('/list_categorias')

# --------------------------------------------------------------------------------------
# Ruta inicial Proveedores
# --------------------------------------------------------------------------------------


@app.route('/list_proveedores', methods=['POST', 'GET'])
def op_proveedores():
    if request.method == 'POST':
        nombre = request.form['nombre']
        insertproveedor = Proveedor(Nom_Proveedor=nombre)
        db.session.add(insertproveedor)
        db.session.commit()
        return redirect('/list_proveedores')
    elif request.method == 'GET':
        proveedores = Proveedor.query.all()
        results = [
            {
                "id": proveedor.id,
                "nombre": proveedor.Nom_Proveedor,
            } for proveedor in proveedores]
        return render_template('Proveedores.html', lista=results)


@app.route('/borrar_proveedor/<proveedor_id>')
def op_proveedor(proveedor_id):
    proveedor = Proveedor.query.get_or_404(proveedor_id)
    db.session.delete(proveedor)
    db.session.commit()
    return redirect('/list_proveedores')


@app.route('/actualizar_proveedor/<proveedor_id>', methods=['GET', 'POST'])
def op_proveedor_update(proveedor_id):
    proveedor = Proveedor.query.get_or_404(proveedor_id)
    response = {
        "id": proveedor.id,
        "nombre": proveedor.Nom_Proveedor
    }
    if request.method == 'GET':
        return render_template('actualizar_proveedor.html', lista=response)
    elif request.method == 'POST':
        proveedor.Nom_Proveedor = request.form['nombre']
        db.session.add(proveedor)
        db.session.commit()
        return redirect('/list_proveedores')
# --------------------------------------------------------------------------------------
# Ruta inicial Productos
# --------------------------------------------------------------------------------------


@app.route('/list_productos', methods=['POST', 'GET'])
def op_productos():
    if request.method == 'POST':
        nombre = request.form['nombre']
        select_categoria = request.form.get('categoria')
        select_marca = request.form.get('marca')
        select_proveedor = request.form.get('proveedor')
        insertproducto = Producto(Nom_Producto=nombre, FK_Marca=select_marca,
                                  FK_Categoria=select_categoria, FK_Proveedor=select_proveedor)
        db.session.add(insertproducto)
        db.session.commit()
        return redirect('/list_productos')
    elif request.method == 'GET':
        productos = Producto.query.all()
        prod = results = [
            {
                "id": producto.id,
                "nombre": producto.Nom_Producto,
                "id_marca": producto.marca.id,
                "nom_marca": producto.marca.Nom_Marca,
                "id_categoria": producto.categoria.id,
                "nom_categoria": producto.categoria.Nom_Categoria,
                "id_proveedor": producto.proveedor.id,
                "nom_proveedor": producto.proveedor.Nom_Proveedor,
            } for producto in productos]

        marcas = Marca.query.all()
        marc = results = [
            {
                "id_marca": marca.id,
                "nom_marca": marca.Nom_Marca,
            } for marca in marcas]
        categorias = Categoria.query.all()
        cat = results = [
            {
                "id_categoria": categoria.id,
                "nom_categoria": categoria.Nom_Categoria,
            } for categoria in categorias]
        proveedores = Proveedor.query.all()
        prov = results = [
            {
                "id_proveedor": proveedor.id,
                "nom_proveedor": proveedor.Nom_Proveedor,
            } for proveedor in proveedores]
    return render_template('Productos.html', lista=prod, lista1=marc, lista2=cat, lista3=prov)


@app.route('/borrar_producto/<producto_id>')
def op_producto(producto_id):
    producto = Producto.query.get_or_404(producto_id)
    db.session.delete(producto)
    db.session.commit()
    return redirect('/list_productos')


@app.route('/actualizar_producto/<producto_id>', methods=['GET', 'POST'])
def op_producto_update(producto_id):
    producto = Producto.query.get_or_404(producto_id)
    response = {
        "id": producto.id,
        "nombre": producto.Nom_Producto,
        "id_marca": producto.marca.id,
        "nom_marca": producto.marca.Nom_Marca,
        "id_categoria": producto.categoria.id,
        "nom_categoria": producto.categoria.Nom_Categoria,
        "id_proveedor": producto.proveedor.id,
        "nom_proveedor": producto.proveedor.Nom_Proveedor,
    }
    marcas = Marca.query.all()
    marc = results = [
        {
            "id_marca": marca.id,
            "nom_marca": marca.Nom_Marca,
        } for marca in marcas]
    categorias = Categoria.query.all()
    cat = results = [
        {
            "id_categoria": categoria.id,
            "nom_categoria": categoria.Nom_Categoria,
        } for categoria in categorias]
    proveedores = Proveedor.query.all()
    prov = results = [
        {
            "id_proveedor": proveedor.id,
            "nom_proveedor": proveedor.Nom_Proveedor,
        } for proveedor in proveedores]

    if request.method == 'GET':
        return render_template('actualizar_producto.html', lista=response, lista1=marc, lista2=cat, lista3=prov)
    elif request.method == 'POST':
        producto.Nom_Producto = request.form['nombre']
        producto.FK_Categoria = request.form['categoria']
        producto.FK_Marca = request.form['marca']
        producto.FK_Proveedor = request.form['proveedor']
        db.session.add(producto)
        db.session.commit()
        return redirect('/list_productos')


# --------------------------------------------------------------------------------------
# Ruta inicial Roles
# --------------------------------------------------------------------------------------


@app.route('/list_roles', methods=['POST', 'GET'])
def op_roles():
    if request.method == 'POST':
        nombre = request.form['nombre']
        insertrol = Rol(Nom_Rol=nombre)
        db.session.add(insertrol)
        db.session.commit()
        return redirect('/list_roles')
    elif request.method == 'GET':
        roles = Rol.query.all()
        results = [
            {
                "id": rol.id,
                "nombre": rol.Nom_Rol,
            } for rol in roles]
        return render_template('Roles.html', lista=results)


@app.route('/borrar_rol/<rol_id>')
def op_rol(rol_id):
    rol = Rol.query.get_or_404(rol_id)
    db.session.delete(rol)
    db.session.commit()
    return redirect('/list_roles')


@app.route('/actualizar_rol/<rol_id>', methods=['GET', 'POST'])
def op_rol_update(rol_id):
    rol = Rol.query.get_or_404(rol_id)
    response = {
        "id": rol.id,
        "nombre": rol.Nom_Rol
    }
    if request.method == 'GET':
        return render_template('actualizar_rol.html', lista=response)
    elif request.method == 'POST':
        rol.Nom_Rol = request.form['nombre']
        db.session.add(rol)
        db.session.commit()
        return redirect('/list_roles')


# --------------------------------------------------------------------------------------
# Inicialización del servidor para visualizar las páginas
# --------------------------------------------------------------------------------------
if __name__ == '__main__':
    print("starting web server")
    app.run(debug=True, port=3000)
