from flask import Flask, app, render_template, request
from werkzeug.utils import redirect
from Conexion_db import conexion_db
from jinja2 import Template

app = Flask(__name__)

#--------------------------------------------------------------------------------------
# Ruta inicial donde todo inicia luego que el servidor es iniciado
#--------------------------------------------------------------------------------------


@app.route('/')
def index():
    return render_template('/index.html')
    

#--------------------------------------------------------------------------------------
#Estas rutas muestran realizan los queries que muestran la información de lo contenido
#en las bases de datos - READ -
#--------------------------------------------------------------------------------------

@app.route('/list_categorias')
def list_categorias():
        query = "SELECT * from \"Categorias\""
        cn = conexion_db()
        categorias = cn.consultar_db(query)
        return render_template('Categorias.html', lista=categorias)
@app.route('/list_marcas')
def list_marcas():
        query = "SELECT * from \"Marcas\""
        cn = conexion_db()
        marcas = cn.consultar_db(query)
        return render_template('Marcas.html',lista=marcas)
@app.route('/list_proveedores')
def list_proveedores():
        query = "SELECT * from \"Proveedores\""
        cn = conexion_db()
        marcas = cn.consultar_db(query)
        return render_template('Proveedores.html',lista=marcas)

@app.route('/list_productos')
def list_productos():
        query = 'SELECT "Id_Producto","Nom_Producto", "Nom_Categoria","Nom_Marca"'+\
            'FROM public."Productos"'+\
            'JOIN public."Marcas"'+\
            'ON "Productos"."Fk_Marca" = "Marcas"."ID_Marca"'+\
            'JOIN public."Proveedores"'+\
            'ON "Productos"."Fk_Proveedor" = "Proveedores"."Id_Proveedor"'+\
            'JOIN public."Categorias"'+\
            'ON "Productos"."Fk_Categoria" = "Categorias"."Id_Categoria"'
        cn = conexion_db()
        productos = cn.consultar_db(query)
        return render_template('Productos.html',lista=productos)
#--------------------------------------------------------------------------------------
#Estas rutas se realizan la inserción de datos en la base de datos
# - INSERT -
#--------------------------------------------------------------------------------------

@app.route('/add_categoria', methods=['POST'])
def add_categoria():
    if request.method =='POST':
        nombre = request.form['nombre']
        query = "INSERT INTO public.\"Categorias\"(\"Nom_Categoria\") VALUES ('"+nombre+"') "
        cn = conexion_db()
        cn.escribir_db(query)
        return redirect('/list_categorias')

@app.route('/add_marca', methods=['POST'])
def add_marca():
    if request.method =='POST':
        nombre = request.form['nombre']
        query = "INSERT INTO public.\"Marcas\"(\"Nom_Marca\") VALUES ('"+nombre+"') "
        cn = conexion_db()
        cn.escribir_db(query)
        return redirect('/list_marcas')

@app.route('/add_proveedor', methods=['POST'])
def add_proveedor():
    if request.method =='POST':
        nombre = request.form['nombre']
        query = "INSERT INTO public.\"Proveedores\"(\"Nom_Proveedor\") VALUES ('"+nombre+"') "
        cn = conexion_db()
        cn.escribir_db(query)
        return redirect('/list_proveedores')

@app.route('/add_producto', methods=['POST'])
def add_producto():
    if request.method =='POST':
        nombre = request.form['nombre']
        return redirect('/list_productos')

##Este espacio es decicado a realizar pruebas 

@app.route("/suma",methods=["GET","POST"])
def sumar():
	if request.method=="POST":
		num1=request.form.get("num1")
		num2=request.form.get("num2")
		return str(int(num1)+int(num2))
	else:
		return '''<form action="/suma" method="POST">
				<label>N1:</label>
				<input type="text" name="num1"/>
				<label>N2:</label>
				<input type="text" name="num2"/>
                <button type="submit">Guardar</button>
				</form>'''
@app.route("/plantilla")
def plantilla():
    temp2='''<a href="{{url}}">{{enlace}} </a>'''
    return Template(temp2).render(url="http://www.flask.com",enlace="Flask")




#--------------------------------------------------------------------------------------
#Inicialización del servidor para visualizar las páginas
#--------------------------------------------------------------------------------------
if __name__=='__main__':
    print("starting web server")
    app.run(debug=True,port=3000)



