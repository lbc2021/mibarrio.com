import psycopg2


class conexion_db:
    def __init__(self):
        try:
            self.conn = psycopg2.connect(
                host="localhost",
                database="mibarrio.com",
                user="postgres",
                password="root")

            self.cur = self.conn.cursor()  # se crea el objeto cur como atributo
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def consultar_db(self, query):
        # metodo usado para ejecutar queries que retornan informacion
        # (SELECT), la query a ejecutar se pasa como parámetro al metodo
        try:

            self.cur.execute(query)  # ejecutamos la query con el atributo cur
            response = self.cur.fetchall()  # leemos lo que la query retorna
            return response
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return "error"

    def escribir_db(self, query):
        # metodo usado para queries que no retornan datos
        # queries de escritura -> INSERT,DELETE,CREATE
        try:
            print(query)
            self.cur.execute(query)  # ejecutamos la query
            self.conn.commit()  # actualizamos las tablas
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return "error"

    def cerrar_db(self):
        self.cur.close()
        self.conn.close()
